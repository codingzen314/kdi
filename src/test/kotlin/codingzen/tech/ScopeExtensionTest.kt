package codingzen.tech

import kotlinx.coroutines.runBlocking
import org.junit.Test
import strikt.api.expectCatching
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.*
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.dsl.module
import tech.codingzen.kdi.dsl.tracked
import tech.codingzen.kdi.logging.Logger

class ScopeExtensionTest {
  @Test
  fun `get test`(): Unit = runBlocking {
    val spec = Kdi.moduleSpec("scopeId") {}

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .scopeExtensions {
        spec.id.preModule {
          single { ComponentA() }
        }
        spec.id.postModule {
          single { CommonTypeA() }
        }
      }
      .spec(spec)
      .execute { dsl ->
        expectThat(dsl.get<ComponentA>()) {
          isNotNull()
          isA<ComponentA>()
        }
        expectThat(dsl.get<CommonTypeA>()) {
          isNotNull()
          isA<CommonTypeA>()
        }
      }
  }

  @Test
  fun `getAll test`(): Unit = runBlocking {
    val spec = Kdi.modulesSpec("scopeId") {
      +module {
        tracked<CommonType>().instance(CommonTypeB.instance)
      }
    }

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .scopeExtensions {
        spec.id.preModule {
          tracked<CommonType>().instance(CommonTypeA.instance)
        }
        spec.id.postModule {
          tracked<CommonType>().instance(CommonTypeC.instance)
        }
      }
      .spec(spec)
      .execute { dsl ->
        expectThat(dsl.getAll<CommonType>()).containsExactlyInAnyOrder(CommonTypeA.instance, CommonTypeB.instance, CommonTypeC.instance)
      }
  }

  @Test
  fun `preExtender exception`(): Unit = runBlocking {
    val spec = Kdi.moduleSpec("scopeId") {}
    val exception = RuntimeException()
    val notThrown = RuntimeException()
    expectCatching {
      Kdi.appSpec()
        .printLogger(Logger.Level.TRACE)
        .scopeExtensions {
          spec.id.preProvider { throw exception }
        }
        .spec(spec)
        .execute<Unit> { dsl ->
          throw notThrown
        }
    }.isFailure()
      .isA<KdiException>()
      .and {
        get { cause }.isEqualTo(exception)
      }
  }

  @Test
  fun `postExtender exception`(): Unit = runBlocking {
    val spec = Kdi.moduleSpec("scopeId") {}
    val exception = RuntimeException()
    val notThrown = RuntimeException()
    expectCatching {
      Kdi.appSpec()
        .printLogger(Logger.Level.TRACE)
        .scopeExtensions {
          spec.id.postProvider { throw exception }
        }
        .spec(spec)
        .execute<Unit> { dsl ->
          throw notThrown
        }
    }.isFailure()
      .isA<KdiException>()
      .and {
        get { cause }.isEqualTo(exception)
      }
  }

  class ComponentA

  interface CommonType

  class CommonTypeA : CommonType {
    companion object {
      val instance = CommonTypeA()
    }
  }
  class CommonTypeB : CommonType {
    companion object {
      val instance = CommonTypeB()
    }
  }
  class CommonTypeC : CommonType {
    companion object {
      val instance = CommonTypeC()
    }
  }
}