package codingzen.tech

import kotlinx.coroutines.runBlocking
import org.junit.Test
import strikt.api.expectCatching
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.*
import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.dsl.tracked
import tech.codingzen.kdi.logging.Logger

class ScopeDslTest {
  @Test
  fun `get test`(): Unit = runBlocking {
    val spec = Kdi.scopeSpec("scopeId")
      .noBootstrapper()
      .module {
        single { ComponentA() }
      }
      .build()

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .spec(spec)
      .execute { dsl ->
        expectThat(dsl.get<ComponentA>()) {
          isNotNull()
          isA<ComponentA>()
        }
        expectThrows<KdiException> { dsl.get<ComponentA>("dne") }
      }
  }

  @Test
  fun `getOrNull test`(): Unit = runBlocking {
    val spec = Kdi.scopeSpec("scopeId")
      .noBootstrapper()
      .module {
        single { ComponentA() }
      }
      .build()

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .spec(spec)
      .execute { dsl ->
        expectThat(dsl.getOrNull<ComponentA>()) {
          isNotNull()
          isA<ComponentA>()
        }

        expectThat(dsl.getOrNull<ComponentA>("dne")).isNull()
      }
  }

  @Test
  fun `get lower level dependency test`(): Unit = runBlocking {
    val spec0 = Kdi.scopeSpec("scopeId0")
      .noBootstrapper()
      .module {
        single {
          ComponentZ(get())
        }
      }
      .build()

    val spec1 = Kdi.scopeSpec("scopeId1")
      .noBootstrapper()
      .module {
        single { 10 }
      }
      .build()

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .specs(spec0, spec1)
      .execute { dsl ->
        expectThrows<KdiException> { dsl.get<ComponentZ>() }
      }
  }

  @Test
  fun `getAll test`(): Unit = runBlocking {
    val spec = Kdi.scopeSpec("scopeId")
      .noBootstrapper()
      .module {
        single { ComponentA() }
        tracked<CommonType>().single { CommonTypeA.instance }
        tracked<CommonType>().instance(CommonTypeB.instance)
      }
      .build()

    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .spec(spec)
      .execute { dsl ->
        expectThat(dsl.getAll<CommonType>()) {
          hasSize(2)
          containsExactlyInAnyOrder(CommonTypeA.instance, CommonTypeB.instance)
        }

        expectThat(dsl.getAll<Int>()).isEmpty()
      }
  }

  @Test
  fun `bootstrapper exception`(): Unit = runBlocking {
    val exception = RuntimeException()
    val spec = Kdi.scopeSpec("scopeId")
      .bootstrapper { throw exception }
      .build()
    val notThrown = RuntimeException()
    expectCatching {
      Kdi.appSpec()
        .printLogger(Logger.Level.TRACE)
        .noScopeExtensions()
        .spec(spec)
        .execute<Unit> { dsl ->
          throw notThrown
        }
    }.isFailure()
      .isA<KdiException>()
      .and {
        get { cause }.isEqualTo(exception)
      }

  }

  class ComponentZ(val x: Int)

  class ComponentA

  interface CommonType

  class CommonTypeA : CommonType {
    companion object {
      val instance = CommonTypeA()
    }
  }
  class CommonTypeB : CommonType {
    companion object {
      val instance = CommonTypeB()
    }
  }
}