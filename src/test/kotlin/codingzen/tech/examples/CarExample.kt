package codingzen.tech.examples

import clojure.lang.PersistentHashMap

//
//import kotlinx.coroutines.runBlocking
//import tech.codingzen.kata.result.Res
//import tech.codingzen.kata.result.ok
//import tech.codingzen.kdi.data_structure.ScopeExecutor
//import tech.codingzen.kdi.dsl.Kdi
//import tech.codingzen.kdi.dsl.execute
//
//fun main(args: Array<String>): Unit {
//  val wheels: Wheels = Wheels.HighEfficiency
//  val engine: Engine = Engine.V8
//  val car = Car(engine, wheels)
//  println(car.specs())
//}
//
//fun withDi() {
//  val rootScope = Kdi.rootScopeBuilder
//    .printLogger()
//    .noBootstrapper()
//    .module {
//      single<Wheels> { Wheels.HighEfficiency }
//      single<Engine> { Engine.V6 }
//      single { Car(get(), get()) }
//    }
//    .noScopeExtensions()
//    .build()
//
//  runBlocking {
//    rootScope.execute { scope ->
//      println(scope.get<Car>().specs())
//      Res.ok(Unit)
//    }
//  }
//}
//
//class Car(
//  private val engine: Engine,
//  private val wheels: Wheels
//) {
//  fun specs(): String = "engine: ${engine.specs()}\nwheels: ${wheels.specs()}"
//}
//
//sealed class Engine {
//  object V6 : Engine()
//  object V8 : Engine()
//  data class TurboV6(val numTurbos: Int): Engine()
//
//  fun specs() = when (this) {
//    is V6 -> "V6 engine"
//    is V8 -> "V8 engine"
//    is TurboV6 -> "V6 engine with $numTurbos turbos"
//  }
//}
//
//sealed class Wheels {
//  object HighEfficiency : Wheels()
//  data class Offroad(val treadThickness: Int): Wheels()
//
//  fun specs() = when (this) {
//    is HighEfficiency -> "HighEfficiency wheels"
//    is Offroad -> "Offroad wheels with tread thickness $treadThickness"
//  }
//}



fun main(args: Array<String>) {
  val map = PersistentHashMap.EMPTY.assoc("a", 1).assoc("b", 2)
  println(map)
}