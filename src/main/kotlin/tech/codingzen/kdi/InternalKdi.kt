package tech.codingzen.kdi

/**
 * Marks code as internal to Kdi and as such is not considered a public api and can/will be changed without notice
 * and without any regard for improper usage in client projects.  Likely the code marked with this annotation is
 * needed for an inline function.
 */
annotation class InternalKdi