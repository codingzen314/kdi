package tech.codingzen.kdi.dsl

import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.BuiltScope
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.PersistentSet

class ScopeDsl(val level: Int, val kdi: Kdi) {
  /**
   *
   *
   * @param T component type to retrieve
   * @param [tag] optional parameter used to identify components with the same FQCN
   * @return component of type T contained in the current scope identified by the optional [tag]
   */
  suspend inline fun <reified T> getOrNull(tag: Tag = Kdi.defaultTag): T? {
    val descriptor = Kdi.descriptor<T>(tag)
    kdi.logger.trace { "Attempting retrieval of component with descriptor: $descriptor" }
    val providerWithMetaOrNull = kdi.scopes[level].creatorMap[descriptor]
    return if (providerWithMetaOrNull != null) {
      kdi.logger.trace { "Retrieved component with descriptor: $descriptor" }
      val (provider, level) = providerWithMetaOrNull
      provider(ScopeDsl(level, kdi)) as T
    } else {
      kdi.logger.trace { "Unable to retrieve component with descriptor: $descriptor" }
      null
    }
  }

  /**
   *
   *
   * @param T component type to retrieve
   * @param [tag] optional parameter used to identify components with the same FQCN
   * @return component of type T contained in the current scope identified by the optional [tag]
   */
  suspend inline fun <reified T> get(tag: Tag = Kdi.defaultTag): T {
    val descriptor = Kdi.descriptor<T>(tag)
    if (descriptor.fqcn == Kdi.fqcn) {
      kdi.logger.trace { "Accessing reference to Kdi" }
      return kdi as T
    }
    return getOrNull<T>(tag) ?: run {
      val msg = "Unable to retrieve component for descriptor: ${Kdi.descriptor<T>(tag)}"
      kdi.logger.error { msg }
      throw KdiException(msg)
    }
  }

  /**
   *
   *
   * @param T component type to retrieve
   * @return all components of type T contained in the current scope
   */
  suspend inline fun <reified T> getAll(): Set<T> {
    val fqcn = T::class.qualifiedName ?: throw IllegalStateException("Attempting to retrieve generic class fqcn")
    kdi.logger.trace { "Attempting retrieval of tracked components with fqcn: $fqcn" }

    return mutableSetOf<T>().apply {
      for ((provider, level) in kdi.scopes[level].trackedCreatorMap[fqcn] ?: PersistentSet.empty()) {
        val component = provider(ScopeDsl(level, kdi)) as T
        add(component)
      }
    }
  }
}