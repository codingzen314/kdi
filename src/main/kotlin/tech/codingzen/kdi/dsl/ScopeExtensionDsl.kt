package tech.codingzen.kdi.dsl

import tech.codingzen.kdi.ScopeExtender
import tech.codingzen.kdi.ScopeId
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.data_structure.ScopeExtenderType
import tech.codingzen.kdi.data_structure.ScopeExtenderType.POST
import tech.codingzen.kdi.data_structure.ScopeExtenderType.PRE
import tech.codingzen.kdi.dsl.module.ModuleDsl
import tech.codingzen.kdi.dsl.module.MultipleModulesDsl

/**
 * Dsl helping with extending a Scope
 */
class ScopeExtensionDsl {
  val extensionMap = mutableMapOf<ScopeId, MutableMap<ScopeExtenderType, MutableList<ScopeExtender>>>()


  /**
   * @param [extender] used to extend the scope identified by the receiver of this function
   * @receiver id of scope to extend
   */
  fun ScopeId.preProvider(extender: ScopeExtender) {
    extensionMap.putIfAbsent(this, mutableMapOf())
    val extenderMap = extensionMap[this]!!
    extenderMap.putIfAbsent(PRE, mutableListOf())
    extenderMap[PRE]!!.add(extender)
  }

  /**
   * @param [block] defines a module added as a pre module to the scope identified by the receiver
   * @receiver id of scope to extend
   */
  fun ScopeId.preModule(block: ModuleDsl.() -> Unit) {
    preProvider { KdiModulesFactory.module(block) }
  }

  /**
   * @param [block] defines module(s) added as a pre module(s) to the scope identified by the receiver
   * @receiver id of scope to extend
   */
  fun ScopeId.preModules(block: MultipleModulesDsl.() -> Unit) {
    preProvider { KdiModulesFactory.modules(block) }
  }

  /**
   * @param [extender] used to extend the scope identified by the receiver of this function
   * @receiver id of scope to extend
   */
  fun ScopeId.postProvider(extender: ScopeExtender) {
    extensionMap.putIfAbsent(this, mutableMapOf())
    val extenderMap = extensionMap[this]!!
    extenderMap.putIfAbsent(POST, mutableListOf())
    extenderMap[POST]!!.add(extender)
  }

  /**
   * @param [block] defines a module added as a post module to the scope identified by the receiver
   * @receiver id of scope to extend
   */
  fun ScopeId.postModule(block: ModuleDsl.() -> Unit) {
    postProvider { KdiModulesFactory.module(block) }
  }

  /**
   * @param [block] defines module(s) added as a post module(s) to the scope identified by the receiver
   * @receiver id of scope to extend
   */
  fun ScopeId.postModules(block: MultipleModulesDsl.() -> Unit) {
    postProvider { KdiModulesFactory.modules(block) }
  }
}