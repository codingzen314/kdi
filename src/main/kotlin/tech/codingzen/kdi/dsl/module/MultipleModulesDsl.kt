package tech.codingzen.kdi.dsl.module

import tech.codingzen.kdi.data_structure.KdiModule

/**
 * Dsl that facilitates the building of a list of KdiModule
 */
class MultipleModulesDsl {
  private val _modules = mutableListOf<KdiModule>()

  val modules: List<KdiModule> get() = _modules.toList()

  operator fun KdiModule.unaryPlus() {
    _modules.add(this)
  }
}