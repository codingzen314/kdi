package tech.codingzen.kdi.dsl.module

import tech.codingzen.kdi.InternalKdi
import tech.codingzen.kdi.Provider
import tech.codingzen.kdi.FQCN
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.*

/**
 * Dsl that facilitates building a KdiModule
 */
class ModuleDsl {
  private var trie = PersistentMap.empty<Descriptor, Provider<Any>>()
  private val hash = this.hashCode()

  @PublishedApi
  @InternalKdi
  internal var tracked = PersistentMap.empty<FQCN, PersistentSet<Provider<Any>>>()

  val module: KdiModule get() = KdiModule.Impl(trie, tracked)

  /**
   * Instance component definition
   *
   * @param [T] type of component
   * @param t instance that is the component
   * @param tag uniquely identifies components with the same fqcn
   */
  inline fun <reified T> instance(t: T, tag: Tag = Kdi.defaultTag): Unit {
    provider<T>(tag, Providers.instance(t))
  }

  /**
   * Single component definition
   *
   * @param [T] type of component
   * @param tag uniquely identifies components with the same fqcn
   * @param provider provides the component instance.  This will only be called once for the lifetime of a Kdi application
   */
  inline fun <reified T> single(tag: Tag = Kdi.defaultTag, crossinline provider: Provider<T>): Unit {
    provider<T>(tag, Providers.single(provider))
  }

  /**
   * Provider component definition
   *
   * @param [T] type of component
   * @param provider provides the component instance.  This will be called each time the component is used as a dependency for another component
   * @param tag uniquely identifies components with the same fqcn
   */
  inline fun <reified T> provider(tag: Tag = Kdi.defaultTag, noinline provider: Provider<T>): Unit {
    val descriptor = Kdi.descriptor<T>(tag)

    @Suppress("UNCHECKED_CAST")
    val cFactory = provider as Provider<Any>
    addCreator(this, descriptor, cFactory)
  }

  @InternalKdi
  fun addCreator(ref: Any, descriptor: Descriptor, provider: Provider<Any>): Unit {
    if (hash == ref.hashCode()) {
      trie = trie.assoc(descriptor, provider)
    }
    else throw IllegalStateException("Only ModuleDsl can call addCreator")
  }

  @InternalKdi
  fun addTracked(ref: Any, fqcn: FQCN, provider: Provider<Any>): Unit {
    if (hash == ref.hashCode()) {
      if (!tracked.containsKey(fqcn)) {
        tracked = tracked.assoc(fqcn, PersistentSet.empty())
      }
      tracked = tracked.assoc(fqcn, tracked[fqcn]!! + provider)
    } else throw IllegalStateException("Only ModuleDsl can call addCreator")
  }
}