package tech.codingzen.kdi.dsl

import tech.codingzen.kdi.Provider
import tech.codingzen.kdi.FQCN
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.Providers
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.dsl.module.ModuleDsl
import tech.codingzen.kdi.dsl.module.MultipleModulesDsl

/**
 * KdiModule builder
 *
 * @param block KdiModule dsl builder
 * @return KdiModule containing all definitions in [block]
 */
fun module(block: ModuleDsl.() -> Unit): KdiModule = ModuleDsl().apply(block).module

/**
 * KdiModule's builder
 *
 * @param block KdiModules dsl builder
 * @return list of KdiModule's defined by block
 */
fun modules(block: MultipleModulesDsl.() -> Unit): List<KdiModule> = MultipleModulesDsl().apply(block).modules

/**
 * Start defining a tracked component
 *
 * @param [A] parent type for the tracked component
 * @return `Tracked<A>` instance that allows for the definition of a component to be registered
 */
inline fun <reified A> ModuleDsl.tracked(): Tracked<A> {
  val fqcn = A::class.qualifiedName
    ?: throw IllegalStateException("Cannot create a tracked component without a qualified name")
  return Tracked(fqcn, this)
}

/**
 * Facilitates the definition of a tracked component
 *
 * @param [A] parent type for the tracked component
 */
class Tracked<A>(val fqcn: FQCN, val moduleDsl: ModuleDsl) {

  /**
   * Instance component definition
   *
   * @param [T] subtype of the parent type [A]
   * @param t instance that is the component
   * @param tag uniquely identifies components with the same fqcn
   */
  inline fun <reified T : A> instance(t: T, tag: Tag = Kdi.defaultTag): Unit {
    provider(Providers.instance(t), tag)
  }

  /**
   * Single component definition
   *
   * @param [T] subtype of the parent type [A]
   * @param tag uniquely identifies components with the same fqcn
   * @param provider provides the component instance.  This will only be called once for the lifetime of a Kdi application
   */
  inline fun <reified T : A> single(tag: Tag = Kdi.defaultTag, crossinline provider: Provider<T>): Unit {
    provider(Providers.single(provider), tag)
  }

  /**
   * Provider component definition
   *
   * @param [T] subtype of the parent type [A]
   * @param provider provides the component instance.  This will be called each time the component is used as a dependency for another component
   * @param tag uniquely identifies components with the same fqcn
   */
  inline fun <reified T : A> provider(noinline provider: Provider<T>, tag: Tag = Kdi.defaultTag): Unit {
    moduleDsl.provider(tag, provider)
    @Suppress("UNCHECKED_CAST")
    val cFactory = provider as Provider<Any>
    moduleDsl.addTracked(moduleDsl, fqcn, cFactory)
  }
}