package tech.codingzen.kdi.dsl.builder.kdi_app

import tech.codingzen.kdi.KdiException
import tech.codingzen.kdi.ScopeExtender
import tech.codingzen.kdi.ScopeId
import tech.codingzen.kdi.data_structure.*
import tech.codingzen.kdi.dsl.ScopeExtensionDsl
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.spec.MultipleSpec
import tech.codingzen.kdi.spec.ScopeSpec

object KdiAppSpecBuilders {
  class LoggerStep(private val name: String) {
    /**
     * Use custom logger for Kdi logging
     *
     * @param logger custom logger
     * @return ScopeExtenderStep of the Kdi application building process
     */
    fun logger(logger: Logger) = ScopeExtenderStep(name, logger)

    /**
     * Use print logger for Kdi logging
     *
     * @param level the level that the print logger will log at
     * @return ScopeExtenderStep of the Kdi application building process
     */
    fun printLogger(level: Logger.Level = Logger.Level.INFO) = logger(Logger.printLogger(level))

    /**
     * Use no logger for Kdi logging
     *
     * @return ScopeExtenderStep of the Kdi application building process
     */
    fun noLogger() = logger(Logger.nothingLogger)
  }

  class ScopeExtenderStep(private val name: String, private val logger: Logger) {
    /**
     * Convenience method for specifying no scope extensions are necessary
     *
     * @return ScopeStep of the Kdi application building process
     */
    fun noScopeExtensions() = scopeExtensions { }

    /**
     * Specify Scope extensions for this Kdi application
     *
     * @param block Scopes extensions builder dsl
     * @return ScopeStep of the Kdi application building process
     */
    fun scopeExtensions(block: ScopeExtensionDsl.() -> Unit): ScopeStep =
      ScopeStep(name, logger, ScopeExtensionDsl().apply(block).extensionMap)
  }

  class ScopeStep(
    private val name: String,
    private val logger: Logger,
    private val scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>
  ) {

    /**
     * Set the ScopeSpec for this Kdi application
     *
     * @param spec for this Kdi application
     * @return FinalStep of the Kdi application building process
     */
    fun spec(spec: ScopeSpec) = FinalStep(name, logger, scopeExtenders, spec)

    /**
     * Set the ScopeSpec for this KdiApplication
     *
     * @param specs multiple specs (at least 1) linked in order (left to right) for this Kdi application
     * @return FinalStep of the Kdi application building process
     * @throws KdiException if no specs are provided
     */
    fun specs(vararg specs: ScopeSpec) =
      when {
        specs.isEmpty() -> throw KdiException("You must provide at least one ScopeSpec to specs()")
        specs.size == 1 -> spec(specs.last())
        else -> spec(MultipleSpec(specs.toList()))
      }
  }

  class FinalStep(
    private val name: String,
    private val logger: Logger,
    private val scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>,
    private val scopeSpec: ScopeSpec
  ) {
    /**
     * Execute a block of code in the context of the built Kdi application
     *
     * @param [T] result type
     * @param executor ScopeExecutor returning a result of type [T]
     * @return the result of applying [executor] to the built Kdi application
     */
    suspend fun <T> execute(executor: ScopeExecutor<T>): T {
      val extensions = KdiExtensions(scopeExtenders)
      val kdi = Kdi(extensions, logger)
      return kdi.execute(scopeSpec, executor)
    }
  }
}