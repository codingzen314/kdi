package tech.codingzen.kdi.dsl.builder.kdi_app

import tech.codingzen.kdi.Provider
import tech.codingzen.kdi.ScopeId
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.dsl.module.ModuleDsl
import tech.codingzen.kdi.dsl.module.MultipleModulesDsl
import tech.codingzen.kdi.spec.BaseSpec

object ScopeSpecBuilders {
  class BootstrapperStep(private val id: ScopeId) {
    private val preModules = mutableListOf<KdiModule>()

    /**
     * Add a pre module
     * @param [module] module to add
     * @return BootstrapperStep of the scope building process
     */
    fun module(module: KdiModule): BootstrapperStep = apply {
      preModules.add(module)
    }

    /**
     * Add a pre-module
     * @param [block] defines a module
     * @return BootstrapperStep of the scope building process
     */
    fun module(block: ModuleDsl.() -> Unit): BootstrapperStep = apply {
      preModules.add(ModuleDsl().apply(block).module)
    }

    /**
     * Add pre-module(s)
     * @param [block] defines multiple module(s)
     * @return BootstrapperStep of the scope building process
     */
    fun modules(block: MultipleModulesDsl.() -> Unit): BootstrapperStep = apply {
      preModules.addAll(MultipleModulesDsl().apply(block).modules)
    }

    /**
     * No bootstrapper defined for this Scope
     * @return ModulesStep of the scope building process
     */
    fun noBootstrapper(): ModulesStep = bootstrapper { KdiModulesFactory.empty }

    /**
     * Use a bootstrapper for this scope
     * @param [provider] bootstrapper provider
     * @return ModulesStep of the scope building process
     */
    fun bootstrapper(provider: Provider<KdiModulesFactory>) = ModulesStep(id, preModules, provider)
  }

  class ModulesStep(
    private val id: ScopeId,
    private val preModules: List<KdiModule>,
    private val bootstrapper: Provider<KdiModulesFactory>
  ) {
    private val postModules = mutableListOf<KdiModule>()

    /**
     * Add a post-module
     * @param [module] module to add
     * @return ModulesStep of the scope building process
     */
    fun module(module: KdiModule): ModulesStep = apply {
      postModules.add(module)
    }

    /**
     * Add a post-module
     * @param [block] defines a module
     * @return ModulesStep of the scope building process
     */
    fun module(block: ModuleDsl.() -> Unit): ModulesStep = apply {
      postModules.add(ModuleDsl().apply(block).module)
    }

    /**
     * Add multiple post-module(s)
     * @param [block] defines multiple module(s)
     * @return ModulesStep of the scope building process
     */
    fun modules(block: MultipleModulesDsl.() -> Unit): ModulesStep = apply {
      postModules.addAll(MultipleModulesDsl().apply(block).modules)
    }

    /**
     * Build a BaseSpec
     * @return the built BaseSpec instance
     */
    fun build(): BaseSpec = BaseSpec(id, preModules, bootstrapper, postModules)
  }
}