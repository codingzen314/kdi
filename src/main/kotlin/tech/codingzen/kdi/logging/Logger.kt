package tech.codingzen.kdi.logging

interface Logger {
  companion object {
    /**
     * Create a logger that prints to std out
     * @param [level] logger level defaulted to [Level.INFO]
     * @return a configured PrintLogger instance
     */
    fun printLogger(level: Level = Level.INFO): Logger = PrintLogger(level)

    /**
     * Logger instance that does nothing
     */
    val nothingLogger: Logger = object: Logger {
      override fun log(level: Level, block: () -> String) {}

      override fun log(level: Level, throwable: Throwable, block: () -> String) {}
    }
  }

  enum class Level {
    OFF,
    ERROR,
    WARN,
    TRACE,
    INFO
  }

  fun log(level: Level, block: () -> String)
  fun log(level: Level, throwable: Throwable, block: () -> String)


  fun error(block: () -> String) {
    log(Level.ERROR, block)
  }

  fun warn(block: () -> String) {
    log(Level.WARN, block)
  }

  fun info(block: () -> String) {
    log(Level.INFO, block)
  }

  fun trace(block: () -> String) {
    log(Level.TRACE, block)
  }

  fun error(block: () -> String, throwable: Throwable) {
    log(Level.ERROR, throwable, block)
  }

  fun warn(block: () -> String, throwable: Throwable) {
    log(Level.WARN, throwable, block)
  }

  fun info(block: () -> String, throwable: Throwable) {
    log(Level.INFO, throwable, block)
  }

  fun trace(block: () -> String, throwable: Throwable) {
    log(Level.TRACE, throwable, block)
  }
}