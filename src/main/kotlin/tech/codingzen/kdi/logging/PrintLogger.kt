package tech.codingzen.kdi.logging

class PrintLogger(val level: Logger.Level = Logger.Level.INFO) : Logger {
  override fun log(level: Logger.Level, block: () -> String) {
    if (level >= this.level) println(block())
  }

  override fun log(level: Logger.Level, throwable: Throwable, block: () -> String) {
    if (level >= this.level) {
      println(block)
      throwable.printStackTrace()
    }
  }
}