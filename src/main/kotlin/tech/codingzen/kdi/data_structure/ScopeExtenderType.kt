package tech.codingzen.kdi.data_structure

enum class ScopeExtenderType {
  /**
   * Add scope extensions before the scope
   */
  PRE,

  /**
   * Add scope extensions after the scope
   */
  POST;
}