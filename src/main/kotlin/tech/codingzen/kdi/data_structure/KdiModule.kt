package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.FQCN
import tech.codingzen.kdi.Provider

/**
 * Contains a collection of Creators and TrackedCreators
 */
interface KdiModule {
  val creatorMap: PersistentMap<Descriptor, Provider<Any>>
  val trackedCreatorMap: PersistentMap<FQCN, PersistentSet<Provider<Any>>>

  /**
   * Default implementation clas for KdiModule
   */
  data class Impl(
    override val creatorMap: PersistentMap<Descriptor, Provider<Any>>,
    override val trackedCreatorMap: PersistentMap<FQCN, PersistentSet<Provider<Any>>>
  ) : KdiModule

  /**
   * Can be used to make any type a KdiModule
   */
  interface Delegate : KdiModule {
    val module: KdiModule

    override val creatorMap: PersistentMap<Descriptor, Provider<Any>>
      get() = module.creatorMap
    override val trackedCreatorMap: PersistentMap<FQCN, PersistentSet<Provider<Any>>>
      get() = module.trackedCreatorMap
  }
}