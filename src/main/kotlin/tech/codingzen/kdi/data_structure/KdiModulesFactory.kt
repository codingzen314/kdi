package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.dsl.module.ModuleDsl
import tech.codingzen.kdi.dsl.module.MultipleModulesDsl

/**
 * Used to create module(s) that will be added to Kdi as a bootstrapper or scope extender
 */
fun interface KdiModulesFactory {
  /**
   * Create a list of KdiModule
   *
   * @return list of KdiModule's
   */
  suspend fun create(): List<KdiModule>

  companion object {
    val empty = KdiModulesFactory { listOf() }
    operator fun invoke(vararg modules: KdiModule) = KdiModulesFactory { modules.toList() }
    operator fun invoke(modules: List<KdiModule>) = KdiModulesFactory { modules }
    fun module(block: ModuleDsl.() -> Unit) = KdiModulesFactory {
      listOf(ModuleDsl().apply(block).module)
    }

    fun modules(block: MultipleModulesDsl.() -> Unit) = KdiModulesFactory {
      MultipleModulesDsl().apply(block).modules
    }
  }

  /**
   * Can be used to make any type a ModulesFactory
   */
  interface Delegate : KdiModulesFactory {
    val kdiModulesFactory: KdiModulesFactory

    override suspend fun create(): List<KdiModule> = kdiModulesFactory.create()
  }
}