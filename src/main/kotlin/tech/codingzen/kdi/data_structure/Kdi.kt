package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.*
import tech.codingzen.kdi.data_structure.ScopeExtenderType.POST
import tech.codingzen.kdi.data_structure.ScopeExtenderType.PRE
import tech.codingzen.kdi.dsl.ScopeDsl
import tech.codingzen.kdi.dsl.builder.kdi_app.KdiAppSpecBuilders
import tech.codingzen.kdi.dsl.builder.kdi_app.ScopeSpecBuilders
import tech.codingzen.kdi.dsl.module.ModuleDsl
import tech.codingzen.kdi.dsl.module.MultipleModulesDsl
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.spec.*
import java.util.*

data class CreatorsDS(val creatorMap: CreatorMap, val trackedMap: TrackedCreators) {
  companion object {
    val empty = CreatorsDS(PersistentMap.empty(), PersistentMap.empty())
  }
}

data class ProviderWithMeta(val provider: Provider<Any>, val level: Int)

data class BuiltScope(
  val creatorMap: PersistentMap<Descriptor, ProviderWithMeta>,
  val trackedCreatorMap: PersistentMap<FQCN, PersistentSet<ProviderWithMeta>>
) {
  companion object {
    val empty = BuiltScope(PersistentMap.empty(), PersistentMap.empty())
  }
}

/**
 * Used to facilitate building Kdi applications, ScopeSpec's, and execution of ScopeExecutor's
 */
class Kdi @InternalKdi constructor(
  private val extensions: KdiExtensions,
  val logger: Logger,
  @InternalKdi
  val scopes: PersistentVector<BuiltScope> = PersistentVector.empty<BuiltScope>() + BuiltScope.empty
) {
  companion object {
    @InternalKdi
    val fqcn = Kdi::class.qualifiedName

    /**
     * default tag for components that do not have a specific tag
     */
    val defaultTag: Tag = "___default-tag___"

    fun appSpec(name: String = "app") = KdiAppSpecBuilders.LoggerStep(name)

    /**
     * Create builder for BaseSpec that allows a bootstrapper to be specified
     *
     * @param id scope id
     * @return builder for BaseSpec at the bootstrapper step
     */
    fun scopeSpec(id: ScopeId) = ScopeSpecBuilders.BootstrapperStep(id)

    /**
     * Convenience function that creates a BaseSpec for a specific KdiModule
     *
     * @param id scope id
     * @param block KdiModule builder dsl
     * @return BaseSpec with no bootstrapper and module built from [block]
     */
    fun moduleSpec(id: ScopeId, block: ModuleDsl.() -> Unit): BaseSpec =
      scopeSpec(id)
        .noBootstrapper()
        .module(block)
        .build()

    /**
     * Convenience function that creates a BaseSpec for multiple KdiModules
     *
     * @param id scope id
     * @param block KdiModules builder dsl
     * @return BaseSpec with no bootstrapper and modules built from [block]
     */
    fun modulesSpec(id: ScopeId, block: MultipleModulesDsl.() -> Unit): BaseSpec =
      scopeSpec(id)
        .noBootstrapper()
        .modules(block)
        .build()

    /**
     * Create a component descriptor
     *
     * @param T component type
     * @param tag uniquely identifies components with the same fqcn.  Optional parameter that defaults to [Kdi.defaultTag]
     * @return descriptor for type [T]
     */
    inline fun <reified T> descriptor(tag: Tag = defaultTag): Descriptor {
      val fqcn = T::class.qualifiedName
        ?: throw IllegalStateException("Cannot create a component without a qualified name")
      return Descriptor(fqcn, tag)
    }
  }

  /**
   * Using the current 'scope' (held by an instance of Kdi) to create a child scope defined by [spec].  The created
   * scope is then accessed via ScopeDsl and [executor].
   *
   * @param T result type
   * @param spec scope specification to be created in the context of this Kdi instance
   * @param executor uses the created scope to compute a result of type T
   * @returns the result of applying [executor] to the built scope defined by [spec]
   */
  suspend fun <T> execute(spec: ScopeSpec, executor: ScopeExecutor<T>): T {
    val builtCreatorsDS = when (spec) {
      is BaseSpec -> createScope(scopes, spec)
      is MultipleSpec -> {
        var acc: PersistentVector<BuiltScope> = scopes
        val stack = Stack<ScopeSpec>().apply { spec.specs.asReversed().forEach { push(it) } }
        while (stack.isNotEmpty()) {
          when (val current = stack.pop()) {
            is BaseSpec -> acc = createScope(acc, current)
            is MultipleSpec -> current.specs.asReversed().forEach { stack.push(it) }
          }
        }
        acc
      }
    }

    return executor.execute(ScopeDsl(builtCreatorsDS.length - 1, Kdi(extensions, logger, builtCreatorsDS)))
  }

  fun BuiltScope.extendWith(level: Int, modules: List<KdiModule>): BuiltScope {
    var (creatorMap, trackedCreators) = this
    for (module in modules) {
      for ((key, provider) in module.creatorMap) {
        if (key in creatorMap) {
          logger.warn { "component with descriptor: $key is being overridden by a later scope!" }
        }
        creatorMap += key to ProviderWithMeta(provider, level)
      }

      for ((key, providers) in module.trackedCreatorMap) {
        trackedCreators = trackedCreators.update(key) { existingProviderWithMetas ->
          if (existingProviderWithMetas == null) providers.map { ProviderWithMeta(it, level) }
          else providers.map { ProviderWithMeta(it, level) } + existingProviderWithMetas
        }
      }
    }

    return BuiltScope(creatorMap, trackedCreators)
  }

  private suspend fun createScope(scopes: PersistentVector<BuiltScope>, base: BaseSpec): PersistentVector<BuiltScope> {
    var currentScopes = scopes

    var builtScope = currentScopes.last.extendWith(currentScopes.length, base.preModules)
    currentScopes += builtScope

    val preExtenders = (extensions.scopeExtensions[base.id]?.get(PRE) ?: listOf())
    if (preExtenders.isNotEmpty()) {
      val scopeDsl = ScopeDsl(currentScopes.length - 1, Kdi(extensions, logger, currentScopes))
      val modules = try {
        preExtenders.flatMap { it(scopeDsl).create() }
      } catch (exc: Exception) {
        throw KdiException("pre ScopeExtender failed", exc)
      }
      builtScope = currentScopes.last.extendWith(currentScopes.length, modules)
      currentScopes += builtScope
    }

    val runtimeModules = try {
      base.bootstrapper(ScopeDsl(currentScopes.length - 1, Kdi(extensions, logger, currentScopes))).create()
    } catch (exc: Exception) {
      throw KdiException("bootstrapper for scope: ${base.id} failed", exc)
    }
    builtScope = currentScopes.last.extendWith(currentScopes.length, runtimeModules)
    currentScopes += builtScope

    builtScope = currentScopes.last.extendWith(currentScopes.length, base.postModules)
    currentScopes += builtScope

    val postExtenders = (extensions.scopeExtensions[base.id]?.get(POST) ?: listOf())
    if (postExtenders.isNotEmpty()) {
      val scopeDsl = ScopeDsl(currentScopes.length - 1, Kdi(extensions, logger, currentScopes))
      val modules = try {
        postExtenders.flatMap { it(scopeDsl).create() }
      } catch (exc: Exception) {
        throw KdiException("pre ScopeExtender failed", exc)
      }
      builtScope = currentScopes.last.extendWith(currentScopes.length, modules)
      currentScopes += builtScope
    }

    return currentScopes

//    val preScope = Scope("${base.id}-pre-scope", base.preModules)
//    for (module in base.preModules) {
//      for (entry in module.creatorMap) {
//        val key = entry.key
//        if (key in creatorMap) {
//          logger.warn { "component with descriptor: $key is being overridden by a later scope!" }
//        }
//        creatorMap += entry
//      }
//
//      for (entry in module.trackedCreatorMap) {
//        trackedCreators = trackedCreators.update(entry.key) {
//          if (it == null) entry.value
//          else entry.value + it
//        }
//      }
//    }
    //TOOD: complete all the collections methods such as add all, update-key
//    val preExtenders = (extensions.scopeExtensions[base.id]?.get(PRE) ?: listOf())
//    val extendedPreScope =
//      if (preExtenders.isEmpty()) preScope
//      else run {
//        val scopeDsl = ScopeDsl(copy(creatorMap, currentTrackedCreators))
//        val modules = try {
//          preExtenders.flatMap { it(scopeDsl).create() }
//        } catch (exc: Exception) {
//          throw KdiException("pre ScopeExtender failed", exc)
//        }
//        Scope("${base.id}-pre-scope-extended", modules)
//      }


//    val runtimeModules = try {
//      base.bootstrapper(ScopeDsl(copy(extendedPreScope), extendedPreScope)).create()
//    } catch (exc: Exception) {
//      throw KdiException("bootstrapper for scope: ${base.id} failed", exc)
//    }
//    val bootstrappedScope = extendedPreScope + Scope("${base.id}-bootstrapped-scope", runtimeModules)

//    val postScope = bootstrappedScope + Scope("${base.id}-post-scope", base.postModules)
//    val postExtenders = (extensions.scopeExtensions[base.id]?.get(POST) ?: listOf())
//    val extendedPostScope =
//      if (postExtenders.isEmpty()) postScope
//      else run {
//        val scopeDsl = ScopeDsl(copy(postScope), postScope)
//        val modules = try {
//          postExtenders.flatMap { it(scopeDsl).create() }
//        } catch (exc: Exception) {
//          throw KdiException("pre ScopeExtender failed", exc)
//        }
//        postScope + Scope("${base.id}-post-scope-extended", modules)
//      }

//    return extendedPostScope
  }
}
