package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.InternalKdi
import java.util.NoSuchElementException

@InternalKdi
sealed class ScopesList : AbstractCollection<ScopesList.Cons>() {
  object Nil : ScopesList()
  class Cons(val scope: Scope, val tail: ScopesList): ScopesList()

  class Iterator(private var current: ScopesList): kotlin.collections.Iterator<Cons> {
    override fun hasNext(): Boolean = current is Cons

    override fun next(): Cons {
      if (!hasNext()) throw NoSuchElementException("No more scopes")
      val head = (current as Cons)
      current = head.tail
      return head
    }
  }

  companion object {
    val nil: ScopesList = Nil
    operator fun invoke(vararg scopes: Scope): ScopesList = scopes.foldRight(nil) { scope, list -> list.cons(scope) }
  }

  fun cons(scope: Scope): ScopesList = Cons(scope, this)

  infix operator fun plus(next: Scope): ScopesList = cons(next)

  override val size: Int get() = fold(0) { acc, _ -> acc + 1}

  override fun iterator(): kotlin.collections.Iterator<ScopesList.Cons> = Iterator(this)
}