package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.InternalKdi
import tech.codingzen.kdi.ScopeExtender
import tech.codingzen.kdi.ScopeId

/**
 * Used as a holder class for any extensions needed during application building
 */
@InternalKdi
data class KdiExtensions(val scopeExtensions: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>)

