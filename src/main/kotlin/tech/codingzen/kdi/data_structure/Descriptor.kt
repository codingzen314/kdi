package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.FQCN
import tech.codingzen.kdi.Tag

/**
 * Descriptor uniquely identifies an element that is available for dependency injection
 *
 * @property fqcn fully qualified class name
 * @property tag discriminator for element's that have the same fqcn
 * @constructor creates a [Descriptor]
 */
data class Descriptor(val fqcn: FQCN, val tag: Tag = Kdi.defaultTag)