package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.InternalKdi
import tech.codingzen.kdi.ScopeId

/**
 * Represents a currently "active" scope that has an id and list of [KdiModule] containing dependencies
 */
@InternalKdi
data class Scope(
  val id: ScopeId,
  val modules: List<KdiModule>
)

