package tech.codingzen.kdi.data_structure

import clojure.lang.IPersistentMap
import clojure.lang.PersistentHashMap
import clojure.lang.SeqIterator

@JvmInline
value class PersistentMap<K, out V> private constructor(private val pmap: IPersistentMap = PersistentHashMap.EMPTY): Iterable<Map.Entry<K, V>> {
  companion object {
    fun <K, V> empty() = PersistentMap<K, V>()
  }

  operator fun plus(entry: Pair<K, @UnsafeVariance V>): PersistentMap<K, V> =
    PersistentMap(pmap.assoc(entry.first, entry.second))

  operator fun plus(entry: Map.Entry<K, @UnsafeVariance V>): PersistentMap<K, V> =
    PersistentMap(pmap.assoc(entry.key, entry.value))

  operator fun plus(other: PersistentMap<K, @UnsafeVariance V>): PersistentMap<K, V> {
    var result = this
    for (entry in other) {
      result += entry
    }
    return result
  }

  fun update(key: K, block: (V?) -> @UnsafeVariance V): PersistentMap<K, V> = assoc(key, block(this[key]))

  fun assoc(key: K, value: @UnsafeVariance V): PersistentMap<K, V> =
    PersistentMap(pmap.assoc(key, value))

  operator fun contains(key: K): Boolean = containsKey(key)

  fun containsKey(key: K): Boolean = pmap.containsKey(key)

  fun containsValue(value: @UnsafeVariance V): Boolean = pmap.contains(value)

  @Suppress("UNCHECKED_CAST")
  operator fun get(key: K): V? = clojure.lang.RT.get(pmap, key) as V?

  fun isEmpty(): Boolean = pmap.count() == 0

  fun isNotEmpty(): Boolean = !isEmpty()

  @Suppress("UNCHECKED_CAST")
  override operator fun iterator() = SeqIterator(pmap.seq()) as Iterator<Map.Entry<K, V>>
}