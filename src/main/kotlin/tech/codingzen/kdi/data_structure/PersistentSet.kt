package tech.codingzen.kdi.data_structure

import clojure.lang.IPersistentSet
import clojure.lang.PersistentHashSet
import clojure.lang.SeqIterator

@JvmInline
value class PersistentSet<out V> private constructor(private val pset: IPersistentSet = PersistentHashSet.EMPTY): Iterable<V> {
  companion object {
    fun <V> empty() = PersistentSet<V>()
  }

  operator fun plus(element: @UnsafeVariance V): PersistentSet<V> = PersistentSet(clojure.lang.RT.conj(pset, element) as IPersistentSet)

  operator fun plus(elements: Iterable<@UnsafeVariance V>): PersistentSet<V> =
    elements.fold(this) { acc, v -> acc + v }

  val size: Int
    get() = pset.count()

  fun contains(element: @UnsafeVariance V): Boolean = pset.contains(element)

  fun isEmpty(): Boolean = size == 0

  fun isNotEmpty(): Boolean = !isEmpty()

  @Suppress("UNCHECKED_CAST")
  override operator fun iterator() = SeqIterator(pset.seq()) as Iterator<V>

  inline fun <W> map(block: (V) -> W): PersistentSet<W> {
    var result = empty<W>()
    for (v in this) {
      result += block(v)
    }
    return result
  }
}