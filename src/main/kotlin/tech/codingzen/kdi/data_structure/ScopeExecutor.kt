package tech.codingzen.kdi.data_structure

import tech.codingzen.kdi.dsl.ScopeDsl

/**
 * @param [T] result type
 */
fun interface ScopeExecutor<out T> {
  /**
   * Execute code within the context of a ScopeDsl
   *
   * @param scopeDsl reference to the current Scope in use
   * @return the result of applying this ScopeExecutor to [scopeDsl]
   */
  suspend fun execute(scopeDsl: ScopeDsl): T
}