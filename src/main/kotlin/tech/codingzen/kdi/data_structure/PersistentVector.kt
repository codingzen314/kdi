package tech.codingzen.kdi.data_structure

import clojure.lang.IPersistentVector
import clojure.lang.SeqIterator

@JvmInline
value class PersistentVector<out V> private constructor(private val pvec: IPersistentVector = clojure.lang.PersistentVector.EMPTY) : Iterable<V> {
  companion object {
    fun <V> empty() = PersistentVector<V>()
  }

  val length: Int get() = pvec.count()

  operator fun plus(element: @UnsafeVariance V): PersistentVector<V> =
    PersistentVector(pvec.cons(element))

  operator fun get(index: Int): V = pvec.nth(index) as V

  fun isEmpty(): Boolean = length == 0
  fun isNotEmpty(): Boolean = !isEmpty()

  val last: V get() =
    if (length == 0) throw NoSuchElementException("tried to get last element of an empty vector")
    else this[length - 1]

  fun lastOrElse(default: @UnsafeVariance V): V =
    if (length == 0) default else this[length - 1]

  @Suppress("UNCHECKED_CAST")
  override operator fun iterator() = SeqIterator(pvec.seq()) as Iterator<V>
}