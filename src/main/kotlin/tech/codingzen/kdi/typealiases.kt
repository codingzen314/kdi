package tech.codingzen.kdi

import tech.codingzen.kdi.data_structure.Descriptor
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.data_structure.PersistentMap
import tech.codingzen.kdi.data_structure.PersistentSet
import tech.codingzen.kdi.dsl.ScopeDsl

/**
 * Creates a component given a Scope
 */
typealias Provider<T> = suspend ScopeDsl.() -> T

/**
 * Represents the map of FQCN --> Set<Creator>
 */
typealias TrackedCreators = PersistentMap<FQCN, PersistentSet<Pair<Provider<Any>, Short>>>

/**
 * Represents a trie data structure where FQCN, Tag --> Creator
 */
typealias CreatorMap = PersistentMap<Descriptor, Pair<Provider<Any>, Short>>

/**
 * Fully Qualified Class Name
 */
typealias FQCN = String

/**
 * Differentiates descriptors with equal FQCN
 */
typealias Tag = Any

typealias ScopeId = String

typealias ScopeExtender = Provider<KdiModulesFactory>