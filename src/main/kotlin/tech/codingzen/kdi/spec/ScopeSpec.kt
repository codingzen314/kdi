package tech.codingzen.kdi.spec

import tech.codingzen.kdi.Provider
import tech.codingzen.kdi.ScopeId
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.logging.Logger

sealed interface ScopeSpecLogger
object InheritLogger : ScopeSpecLogger

@JvmInline
value class SpecifiedLogger(val logger: Logger) : ScopeSpecLogger

sealed interface ScopeSpec {

}

interface BaseSpec : ScopeSpec {
  val id: ScopeId
  val preModules: List<KdiModule>
  val bootstrapper: Provider<KdiModulesFactory>
  val postModules: List<KdiModule>

  companion object {
    operator fun invoke(
      id: ScopeId,
      preModules: List<KdiModule>,
      bootstrapper: Provider<KdiModulesFactory>,
      postModules: List<KdiModule>
    ): BaseSpec = Impl(id, preModules, bootstrapper, postModules)
  }

  class Impl(
    override val id: ScopeId,
    override val preModules: List<KdiModule>,
    override val bootstrapper: Provider<KdiModulesFactory>,
    override val postModules: List<KdiModule>
  ) : BaseSpec

  interface Delegate : BaseSpec {
    val scopeSpec: BaseSpec

    override val id: ScopeId get() = scopeSpec.id
    override val preModules: List<KdiModule> get() = scopeSpec.preModules
    override val bootstrapper: Provider<KdiModulesFactory> get() = scopeSpec.bootstrapper
    override val postModules: List<KdiModule> get() = scopeSpec.postModules
  }
}

interface MultipleSpec : ScopeSpec {
  val specs: List<ScopeSpec>

  companion object {
    operator fun invoke(scopes: List<ScopeSpec>): MultipleSpec = Impl(scopes)
    operator fun invoke(vararg scopes: ScopeSpec): MultipleSpec = Impl(scopes.toList())
  }

  class Impl(override val specs: List<ScopeSpec>) : MultipleSpec

  interface Delegate : MultipleSpec {
    val multiple: MultipleSpec

    override val specs: List<ScopeSpec>
      get() = multiple.specs
  }
}

