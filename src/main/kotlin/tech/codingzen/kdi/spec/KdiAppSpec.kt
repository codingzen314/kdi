package tech.codingzen.kdi.spec

import tech.codingzen.kdi.ScopeExtender
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.ScopeId
import tech.codingzen.kdi.data_structure.ScopeExtenderType

interface KdiAppSpec {
  val logger: Logger
  val name: String
  val scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>
  val scopeSpec: ScopeSpec

  companion object {
    operator fun invoke(
      logger: Logger,
      name: String,
      scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>,
      scopeSpec: ScopeSpec
    ): KdiAppSpec = Impl(logger, name, scopeExtenders, scopeSpec)
  }

  class Impl(
    override val logger: Logger,
    override val name: String,
    override val scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>>,
    override val scopeSpec: ScopeSpec
  ) : KdiAppSpec

  interface Delegate : KdiAppSpec {
    val kdiAppSpec: KdiAppSpec

    override val logger: Logger get() = kdiAppSpec.logger
    override val name: String get() = kdiAppSpec.name
    override val scopeExtenders: Map<ScopeId, Map<ScopeExtenderType, List<ScopeExtender>>> get() = kdiAppSpec.scopeExtenders
    override val scopeSpec: ScopeSpec get() = kdiAppSpec.scopeSpec
  }
}