## What is Kdi?
An extremely lightweight dependency-injection, DI, library for Kotlin.  Instead of annotations and complex
annotation processors, Kdi uses kotlin _inline functions_, _reified types_, along with dsl's to provide a straightforward
DI library

## Why Kdi
Let's start with a straightforward example using the code below as a start
```kotlin
fun main(args: Array<String>): Unit {
  val wheels: Wheels = Wheels.HighEfficiency
  val engine: Engine = TurboV6(2)
  val car = Car(engine, wheels)
  println(car.specs())
}
```

<details>
    <summary>Car class hierarchy</summary>

```kotlin
class Car(
  private val engine: Engine,
  private val wheels: Wheels
) {
  fun specs(): String = "engine: ${engine.specs()}\nwheels: ${wheels.specs()}"
}

sealed class Engine {
  object V6 : Engine()
  object V8 : Engine()
  data class TurboV6(val numTurbos: Int): Engine()

  fun specs() = when (this) {
    is V6 -> "V6 engine"
    is V8 -> "V8 engine"
    is TurboV6 -> "V6 engine with $numTurbos turbos"
  }
}

sealed class Wheels {
  object HighEfficiency : Wheels()
  data class Offroad(val treadThickness: Int): Wheels()

  fun specs() = when (this) {
    is HighEfficiency -> "HighEfficiency wheels"
    is Offroad -> "Offroad wheels with tread thickness $treadThickness"
  }
}
```
</details>

Let's see how a Kdi module helps in the construction of a `Car`
```kotlin
module {
  single<Wheels> { Wheels.HighEfficiency }
  single<Engine> { TurboV6(2) }
  single { Car(get(), get()) }
}
```

Notice that the invocation of the `Car` constructor does not directly use `Wheels.HighEfficieny` nor calls the constructor
of `TurboV6`.  Instead, retrieval of the dependencies is done via the `get()` method.  You can see that adding Kdi into 
the code does not add too much extra code considering the benefits described above. 

## Kdi Dsl
Kdi provides a set of dsl's for scope creation, module creation, component retrieval, and scope extension.  Let's explore
these dsl's

### Module Dsl
A module is a set of component definitions.  Use the `module { }` syntax to create a module.  The module has functions
that facilitate component definition.
- `instance()` component definition when the instance of the component type has been created.  Often used in a
  `KdiModulesCreator` implementation for bootstrapping a scope.
- `single { }` singleton component definition.  Only calls the creator function once. 
- `creator { }` generic component definition to allow extension of the Kdi library

Note that all the functions above take an optional `tag: Tag = Kdi.defaultTag` parameter which _should_ used to uniquely
identify components of the same type.  

Sometimes we need to retrieve a set of components that have a common type.  There is a `tracked<CommonTypeHere>()` 
function that provides a dsl allowing for the creation of a component as described above.  All component definitions 
created via _tracked<>()_ are available for retrieval both individually and as a _Set<CommonTypeHere>_

TODO: example of all different types of component creation

### Component Retrieval Dsl
`typealias Creator<T> = suspend Scope.() -> T` describes the "Component Retrieval Dsl".  Scopes allow retrieval of components defined 
in module(s).  
- `get<T>()` retrieves a component of type _T_ or throws an exception if the component is not found.  An optional 
  `tag: Tag = Kdi.defaultTag` parameter is used in addition to the type _T_ to uniquely identify the component to retrieve.
- `getOrNull<T>()` retrieves a component of type _T_ or returns null if a component is not found.  An optional
  `tag: Tag = Kdi.defaultTag` parameter is used in addition to the type _T_ to uniquely identify the component to retrieve.
- `getAll<T>()` retrieves all components of type _T_ as a `Set<T>`.

### Spec (Scope) Creation Dsl
A Kdi module define sets of component definitions but "when" these component definitions (defined in the module) are available, 
depends on the spec the module is added too.  Let's see a couple ways to create a spec
```kotlin
val spec: BaseSpec = Kdi.scopeSpec("scopeId")
  .noBootstrapper()
  .module {
    single { ComponentA() }
  }
  .build()

val spec0: BaseSpec = ...
val spec1: MultipleSpec = MultipleSpec()        
val specs: MultipleSpec = MultipleSpec(spec0, spec1, ...)
```

A MultipleSpec is a list of specs allows for flexible spec hierarchies to be built.  More on this later on!

A BaseSpec has 
- pre modules : module(s) containing component definitions that can be used by bootstrapper and post modules
- bootstrapper : gives the ability to built modules at runtime.  Can use components defined in pre modules
- post modules : module(s) containing component definitions that can use both pre modules and modules built by bootstrapper

See the following picture for a clear explanation of which components are available to downstream components:

![Component Availability](./docs/resources/data-structure-Page-2.drawio.png)

Kdi follows the simple rule: "any component defined in an ancestor scope is available for injection"!

At this point you may be asking how an application can be coded using Kdi?  Let's take a look at how a typical breakdown 
of an application can be accomplished using Kdi.  Let's see the spec definitions:

```kotlin
val applicationSpecs = MultipleSpec(
  Kdi.scopeSpec("root")
    .noBootstrapper()
    .module {
      single { DateTimeUtils() }
      single { jacksonObjectMapper() }
      single { SystemEnv() }
    }
    .build(),
  Kdi.scopeSpec("config")
    .bootstrapper { ConfigSourcesBootstrapper(get()) }
    .module {
      single { MyProcessConfig(get()) }  
      // imagine get() resulting in the configuration sources which can be used to build an instance of MyProcessConfig
    }
    .build()
)
```

Next up is creating a Kdi application that can utilize the `applicationSpecs` to do something ... Start up a web server, 
compute a value and exit the application, start up coroutines, ... whatever you can imagine.

```kotlin
Kdi.appSpec()
  .printLogger(Logger.Level.TRACE)
  .noScopeExtensions()
  .spec(applicationSpecs)
  .execute { scopeDsl ->
    // utilize scopeDsl to retrive component definitions and then do whatever your app needs to do here
  }
```

We will talk about scope extensions later.  The important thing to notice is the `execute` function that takes in a 
ScopeExecutor
```kotlin
/**
 * @param [T] result type
 */
fun interface ScopeExecutor<out T> {
  /**
   * Execute code within the context of a ScopeDsl
   *
   * @param scopeDsl reference to the current Scope in use
   * @return the result of applying this ScopeExecutor to [scopeDsl]
   */
  suspend fun execute(scopeDsl: ScopeDsl): T
}
```

The ScopeExecutor gives you access to the `ScopeDsl` (think DI "container") and you can retrieve components from the 
ScopeDsl to do whatever the app needs to do.

### Extending Kdi
TODO: this section

Kdi allows for both compile time specs and runtime extension of creating specs (think request scoped components)